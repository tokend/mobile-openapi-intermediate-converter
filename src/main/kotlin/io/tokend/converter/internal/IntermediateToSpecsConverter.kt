package io.tokend.converter.internal

import java.io.File

/**
 * Converts intermediate built by OpenAPI generator (by Gena)
 * to the specs accepted by final Ruby generators (by Dima).
 */
class IntermediateToSpecsConverter {
    fun convert(
        intermediateFilePath: String,
        outputDirectoryPath: String,
        ignoredKeys: Set<String> = emptySet(),
    ) {
        println("Converting intermediate '$intermediateFilePath' to specs '$outputDirectoryPath'...\n")

        val openApi = OpenApiIntermediateParser().parse(intermediateFilePath, ignoredKeys)
        val specs = YamlSpecsGenerator(openApi).generate()

        File(outputDirectoryPath).deleteRecursively()

        File("$outputDirectoryPath/inner").mkdirs()
        File("$outputDirectoryPath/resources").mkdirs()

        specs.inner.forEach { (name, content) ->
            File("$outputDirectoryPath/inner/$name.yaml").outputStream().use { fileOutputStream ->
                fileOutputStream.write(content.toByteArray())
            }
        }

        specs.resources.forEach { (name, content) ->
            File("$outputDirectoryPath/resources/$name.yaml").outputStream().use { fileOutputStream ->
                fileOutputStream.write(content.toByteArray())
            }
        }

        println()

        println("✅ Done!")
    }
}