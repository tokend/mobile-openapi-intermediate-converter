package io.tokend.converter.internal.model

class OpenApi(
        val resourcesMap: Map<String, Resource>,
        val keysMap: Map<String, ResourceKey>,
        val innersMap: Map<String, InnerEntity>
)