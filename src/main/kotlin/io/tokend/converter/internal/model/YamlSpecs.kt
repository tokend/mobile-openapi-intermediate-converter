package io.tokend.converter.internal.model

class YamlSpecs(
        val resources: Map<String, String>,
        val inner: Map<String, String>
)