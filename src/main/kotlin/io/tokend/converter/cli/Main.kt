package io.tokend.converter.cli

import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.mainBody
import io.tokend.converter.internal.IntermediateToSpecsConverter

fun main(args: Array<String>): Unit = mainBody {
    ArgParser(args).parseInto(::Arguments).also { arguments ->
        IntermediateToSpecsConverter()
            .convert(
                intermediateFilePath = arguments.intermediateFilePath,
                outputDirectoryPath = arguments.outputDirectoryPath,
                ignoredKeys = arguments.ignoredKeys.toSet()
            )
    }
}